import pygame 

def move_and_collide(ent,move_vector,layer):
    collision_types = {'top':False,'bottom':False,'right':False,'left':False}  
    ent.collision_mask.x += move_vector.x
    hit_list = ent.get_colliding_entitys_layer(layer)
    for h_ent in hit_list:
        if move_vector.x > 0:
            ent.collision_mask.right = h_ent.collision_mask.left
            collision_types['right'] = True
            move_vector.x=0

        elif move_vector.x < 0:
            ent.collision_mask.left = h_ent.collision_mask.right
            collision_types['left'] = True
            move_vector.x=0
        
    ent.collision_mask.y += move_vector.y
    hit_list = ent.get_colliding_entitys_layer(layer)

        for h_ent in hit_list:
            if move_vector.y > 0:
                ent.collision_mask.bottom = h_ent.collision_mask.top
                collision_types['bottom'] = True
                move_vector.y=0

            elif move_vector.y < 0:
                ent.collision_mask.top = h_ent.collision_mask.bottom
                collision_types['top'] = True
                move_vector.y=0
        
        ent.pos.x=ent.collision_mask.x
        ent.pos.y=ent.collision_mask.y
        return collision_types

def move(ent,move_vector):
    ent.pos.x += move_vector.x
    ent.collision_mask.y += move_vector.y
    ent.pos.x=ent.collision_mask.x
    ent.pos.y=ent.collision_mask.y
        