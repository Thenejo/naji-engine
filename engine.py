import pygame,math

entitys=[]
rooms=[]
layers=[]

class room:
    def __init__(self,name):
        self.name=name
        self.id="#"+name+str(len(rooms))
        self.layers=[]
        rooms.append(self)
    
    def add_new_layer(self,layer):
        self.layers.append(layer)

    def render_room(self,screen):
        rendering_orders=[[],[],[],[]]
        #ordering the render orders
        for lyr in self.layers:
            rendering_orders[lyr.depht].append(lyr)
        
        for r_order in rendering_orders:
            for r_lyr in r_order:
                r_lyr.render_layer(screen)

class Vector2:
    def __init__(self,x,y):
        self.x=x
        self.y=y 

class entity:

        def __init__(self,pos,sprite,name,layer,render_order=0,tag=""):
            self.id="@"+name+str(len(entitys))
            self.tag=tag
            self.name=name
            self.layer=layer 
            self.render_order=render_order
            self.pos=pos
            self.sprite=sprite
            self.rot=0
            self.scale=Vector2(1,1)
            self.collision_mask=pygame.Rect(self.pos.x,self.pos.y,self.sprite.get_width(),self.sprite.get_height())
            entitys.append(self)
            layer.add_child_entity(self)

        def render_self(self,screen):
            screen.blit(self.sprite,(self.pos.x,self.pos.y))

        def render_collision_mask(self,screen,color):
            pygame.draw.rect(screen,color,self.collision_mask)

        def get_colliding_entitys_layer(self,layer):
            result=[]
            for e in entitys:
                if e.collision_mask.colliderect(self.collision_mask) and e!=self and e.layer==layer:
                    result.append(e)
            return result

class layer:

    def __init__(self,name,depht):
        self.name=name
        self.depht=depht
        self.id="$"+name+str(len(layers))
        self.childs=[]
        layers.append(self)

    def render_layer(self,screen):
        rendering_orders=[[],[],[],[]]
        #ordering the render orders
        for ent in self.get_childs():
            rendering_orders[ent.render_order].append(ent)
        
        for r_order in rendering_orders:
            for r_ent in r_order:
                screen.blit(r_ent.sprite,(r_ent.pos.x,r_ent.pos.y))
        
    def get_childs(self):
        return self.childs

    def add_child_entity(self,ent):
        self.childs.append(ent)
           